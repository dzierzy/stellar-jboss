<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>

<span>Planetary Systems</span>
<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Star</th>
            <th>Distance</th>
        </tr>
    </thead>

    <tbody>

        <c:forEach items="${requestScope.systems}" var="s">
        <tr>
            <td><a href="./planets?systemId=${s.id}">${s.name}</a></td>
            <td>${s.star}</td>
            <td>${s.distance}</td>
        </tr>
        </c:forEach>


    </tbody>
</table>

<jsp:include page="footer.jsp"/>


