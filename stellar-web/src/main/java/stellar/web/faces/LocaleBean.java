package stellar.web.faces;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@ManagedBean
@SessionScoped
public class LocaleBean {

    private String language = FacesContext
            .getCurrentInstance()
            .getViewRoot()
            .getLocale()
            .getLanguage();

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
        FacesContext
                .getCurrentInstance()
                .getViewRoot()
                .setLocale(new Locale(language));
    }

    public List<Locale> getSupportedLocales(){
        Iterator<Locale> itr =
        FacesContext
                .getCurrentInstance()
                .getApplication()
                .getSupportedLocales();
        List<Locale> locales = new ArrayList<>();
        while (itr.hasNext()){
            locales.add(itr.next());
        }
        locales.add(
                FacesContext.getCurrentInstance()
                .getApplication()
                .getDefaultLocale()
        );
        return locales;
    }
}
