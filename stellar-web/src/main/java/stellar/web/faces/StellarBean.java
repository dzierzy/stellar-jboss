package stellar.web.faces;

import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarService;
import stellar.service.impl.StellarServiceImpl;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

@ManagedBean
public class StellarBean {

    //@ManagedProperty("#{stellarService}")
    @Inject
    private StellarService service;

    @ManagedProperty("#{searchBean}")
    private SearchBean searchBean;




    public List<PlanetarySystem> getPlanetarySystems() {
        if (searchBean.isPhrasePresent()) {
            return service.getSystemsByName(searchBean.getPhrase());
        } else {
            return service.getSystems();
        }
    }

    public PlanetarySystem getPlanetarySystem(int systemId) {
        return service.getSystemById(systemId);
    }

    public List<Planet> getPlanets() {

        String systemIdString =
        FacesContext
                .getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("systemId");

        int systemId = Integer.parseInt(systemIdString);
        return service.getPlanets(service.getSystemById(systemId));

    }

    public StellarService getService() {
        return service;
    }

    public void setService(StellarService service) {
        this.service = service;
    }


    public SearchBean getSearchBean() {
        return searchBean;
    }

    public void setSearchBean(SearchBean searchBean) {
        this.searchBean = searchBean;
    }
}
