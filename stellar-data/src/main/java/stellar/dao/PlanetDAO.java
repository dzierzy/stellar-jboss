package stellar.dao;

import stellar.entities.Page;
import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;

import java.util.List;

public interface PlanetDAO {

    List<Planet> getAllPlanets();

    List<Planet> getPlanetsBySystem(PlanetarySystem system);

    List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like);

    Page<Planet> getPlanetsPage(PlanetarySystem s, int pageNo, int pageSize);

    Planet getPlanetById(int id);

    Planet addPlanet(Planet p);

}
